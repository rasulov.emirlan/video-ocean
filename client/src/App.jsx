import CategoryButton from "./components/UI/CategoryButton";
import Input from "./components/UI/Input";
import SubButton from "./components/UI/SubButton";
import "./styles/App.scss";

function App() {
	return (
		<div className="App">
			<SubButton className="subBtn">subscribe</SubButton>

			<CategoryButton>Category</CategoryButton>
			<Input type="password" margin="10px" placeholder="type here..." />
		</div>
	);
}

export default App;

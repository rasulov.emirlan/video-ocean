import React from "react";
import "../../styles/UI/Button.scss";

const CategoryButton = ({ children, img, imgAlt, onClick }) => {
	return (
		<button className="categoryBtn" onClick={onClick}>
			<img src={img} alt={imgAlt} />
			{children}
		</button>
	);
};

export default CategoryButton;

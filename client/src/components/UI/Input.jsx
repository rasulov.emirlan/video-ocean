import React from "react";
import "../../styles/UI/Input.scss";
const Input = ({ width, margin, setValue, type, placeholder }) => {
	return (
		<input
			style={{ width: width, margin: margin }}
			type={type}
			className="Input"
			onChange={(e) => setValue(e.target.value)}
			placeholder={placeholder}></input>
	);
};

export default Input;

import React from "react";
import "../../styles/UI/Button.scss";

const SubButton = ({ children, onClick, disabled }) => {
	return (
		<button
			className={`${disabled ? "bubBtnPressed" : "subBtn"}`}
			onClick={onClick}
			disabled={disabled}>
			{children}
		</button>
	);
};
export default SubButton;
